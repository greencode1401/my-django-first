from django.core.paginator import Paginator
from django.views.generic import ListView, DetailView
from django.shortcuts import render, get_object_or_404 , get_list_or_404
# from django.http import HttpResponse, JsonResponse
from .models import Article ,Category
# Create your views here.
#def home(request, page=1):
 #       articles_list=Article.objects.published()
#        paginator = Paginator(articles_list, 4)
#        articles = paginator.get_page(page)
#        context = {
#             "articles": articles,
 #       }
 #       return render(request,"blog/home.html" , context)
class ArticleList(ListView):
    #model = Article
    #template_name = "blog/home.html"
    #context_object_name ="articles"
    queryset = Article.objects.published()
    paginate_by = 2

#def detail(request, slug):
 #   context = {
  #      "article": get_object_or_404(Article, slug=slug, status="p"),
   #     "category": Category.objects.filter(status=True)
    #}
    #return render(request,"blog/detail.html", context)
class ArticleDetail(DetailView):
    def get_object(self):
        slug = self.kwargs.get('slug')
        return get_object_or_404(Article.objects.published(), slug=slug)


class CatergoryList(ListView):
    paginate_by = 2
    template_name = 'blog/category_obj.html'

    def get_queryset(self):
        global category
        slug = self.kwargs.get('slug')
        category = get_object_or_404(Category.objects.active(), slug=slug)
        return category.articles.published()
           
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['category'] = category
        return context