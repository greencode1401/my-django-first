from django.contrib.admin import AdminSite
from django.urls import path
from .views import ArticleList, CatergoryList ,ArticleDetail
 #,api

app_name = "blog"
urlpatterns = [
    path('', ArticleList.as_view(), name="home"),
    path('page/<int:page>', ArticleList.as_view(), name="home"),
    path('article/<slug:slug>', ArticleDetail.as_view(), name="detail"),
    path('category/<slug:slug>', CatergoryList.as_view(), name="category"),
   # path('api', api, name="api"),
]

